﻿// ======================================================================
// 
//           Copyright (C) 2019-2020 湖南心莱信息科技有限公司
//           All rights reserved
// 
//           filename : CommonAppService.cs
//           description :
// 
//           created by 雪雁 at  2019-06-14 11:22
//           开发文档: docs.xin-lai.com
//           公众号教程：magiccodes
//           QQ群：85318032（编程交流）
//           Blog：http://www.cnblogs.com/codelove/
//           Home：http://xin-lai.com
// 
// ======================================================================

using System;
using System.Collections.Generic;
using Abp.Configuration;
using Abp.Reflection.Extensions;
using Magicodes.Admin.Common.Dto;
using Magicodes.Admin.Core.Custom;

namespace Magicodes.Admin.Common
{
    /// <summary>
    ///     通用服务
    /// </summary>
    public class CommonAppService : AppServiceBase, ICommonAppService
    {
        private readonly ISettingManager _settingManager;


        public CommonAppService(ISettingManager settingManager)
        {
            _settingManager = settingManager;
        }

        /// <summary>
        ///     获取枚举值列表
        /// </summary>
        /// <returns></returns>
        public List<GetEnumValuesListDto> GetEnumValuesList(GetEnumValuesListInput input)
        {
            Type type = null;
            if (input.FullName.Contains("Magicodes.Admin.Core.Custom"))
                type = typeof(AppCoreModule).GetAssembly().GetType(input.FullName);
            else
                type = typeof(AdminCoreModule).GetAssembly().GetType(input.FullName);
            //var type = typeof(AdminCoreModule).GetAssembly().GetType(input.FullName);
            if (!type.IsEnum) return null;

            var names = Enum.GetNames(type);
            var values = Enum.GetValues(type);
            var list = new List<GetEnumValuesListDto>();
            var index = 0;
            foreach (var value in values)
            {
                list.Add(new GetEnumValuesListDto
                {
                    DisplayName = L(names[index]),
                    Value = Convert.ToInt32(value)
                });
                index++;
            }

            return list;
        }
    }
}